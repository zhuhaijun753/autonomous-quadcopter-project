// for ROS and opencv connections
#include <ros/ros.h>
#include <image_transport/image_transport.h>
#include <cv_bridge/cv_bridge.h>
#include <sensor_msgs/image_encodings.h>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>
// for img processing 
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"

#define PI  3.14159265
#define tresholdH 65

// encodings in the enc namespace to be used later.
namespace enc = sensor_msgs::image_encodings;
using namespace cv;
using namespace std;

//Declare a string with the name of the window that we will create using OpenCV where processed images will be displayed.
static const char WINDOW[] = "Image Processed corridor";

//Use method of ImageTransport to create image publisher
image_transport::Publisher pub;
// checking between two lines the crossing point
Point2f * crossPoint(Vec4i l1, Vec4i l2)
{
	Point2f* cP;
	cP= new Point2f;
	Point2f o1 (l1[0], l1[1]) ; // start point line1
        Point2f p1 (l1[2], l1[3]) ; // end point line 1
        Point2f o2 (l2[0], l2[1]) ; // start point line2
        Point2f p2 (l2[2], l2[3]) ; // end point line2
        Point2f x = o2 - o1;
        Point2f d1 = p1 - o1;
        Point2f d2 = p2 - o2;
        float cross = d1.x*d2.y - d1.y*d2.x;
        if (abs(cross) > 1e-8)
        	{
            	double t1 = (x.x * d2.y - x.y * d2.x)/cross;
                * cP= o1 + d1 * t1;
//		printf("x'%g y'%g", cP.x,cP.y);
		//return  cP;	
		}else{
		Point2f cPt(-1,-1);
		*cP=cPt; 
//		return cP;
		}
return cP;
}

// serach for the corridor end/ vanishing point
Mat& corridorProces(Mat& result)
{
Mat src=result.clone();
cvtColor( src, src, CV_RGB2GRAY );
Canny( src, src, 50, 200, 3 );
vector<Vec4i> p_lines;
HoughLinesP( src, p_lines, 1, CV_PI/180, tresholdH, 40, 15 );

for (int i=0; i<p_lines.size(); )
	{
	Vec4i l=p_lines[i];
	float the=atan2(l[1]-l[3],l[2]-l[0])*180/PI;
	if ((the>-2 && the<2)||(abs(the)<100&&abs(the)>80)||(abs(the)<185&&abs(the)>175)||(abs(the)<280&&abs(the)>250))
		{
		p_lines.erase(p_lines.begin()+i);	
		}else{
		i++;
		line( result, Point(l[0], l[1]), Point(l[2], l[3]), Scalar(255,0,0), 1);
		}
	}
int sizeX=src.rows/22, sizeY=src.cols/33;
if (p_lines.size()>1 && sizeX>0 && sizeY>0)
	{
	int imgGrid[22][30];    // grid of crossing points              
        for (int i=0;i<22;i++) // initialization
	        {
                for (int j=0;j<30;j++) imgGrid[i][j]=0;
                }
	Point2f* cP;

	if (p_lines.size()==2)
		{
		cP=crossPoint(p_lines[0],p_lines[1]);
		if (cP->x>0.0 && cP->y>0.0 && cP->x<sizeX*22.0 && cP->y<sizeY*30.0)
			{
			imgGrid[int(cP->x/sizeX)][int(cP->y/sizeY)]++;
			}			
	
		}else{
		for (int i=0;i<p_lines.size()-1;i++)
			{	
			for (int j=i+1; j<p_lines.size(); j++)
				{
				cP=crossPoint(p_lines[i],p_lines[j]);
//				printf("x%g  y%g \n",cP.x,cP.y);	
		                if (cP->x>0.0 && cP->y>0.0 && cP->x<sizeX*22.0 && cP->y<sizeY*30.0)
                		        {
		                        circle(result,cvPoint(int(cP->x),int(cP->y)),3,Scalar(220,0,0),-1,8,0);
					imgGrid[int(cP->x/sizeX)][int(cP->y/sizeY)]++;
                		        }
				}
			}
		}
	delete cP;
	int countP=0,maxi=0, maxj=0;
        for (int i=0;i<22;i++)
	        {
                for (int j=0;j<11;j++)
         	       {
//			printf("%d ", imgGrid[i][j]);
                       if (countP<imgGrid[i][j])
                       		{
                                countP=imgGrid[i][j];
                                maxi=i;
                                maxj=j;
                                }
                       }
//		printf("\n");
               	}
	if (countP>0) 
        	{
                Point vanishP1 = cvPoint(maxi*sizeX,maxj*sizeY), vanishP2 = cvPoint((maxi+1)*sizeX,(maxj+1)*sizeY);
                CvScalar red= CV_RGB(220,0,0);                      
                rectangle(result,vanishP1,vanishP2,red,2,8,0);                
                }else{
		printf("NO vanishing point on the img \n");
		} 		
	}else{
	printf("NOT enough lines \n");
	}
	
return result;
}






//---------------------------------------------------------------------------------------
// function is called everytime a new image is published
void imageCallback(const sensor_msgs::ImageConstPtr& original_image)
{
	//Convert from the ROS image message to a CvImage suitable for working with OpenCV for processing
	cv_bridge::CvImagePtr cv_ptr;	
	try
	{
		//Always copy, returning a mutable CvImage. OpenCV expects color images to use BGR channel order.
		cv_ptr = cv_bridge::toCvCopy(original_image, enc::BGR8);
	}
	catch (cv_bridge::Exception& e)
	{
		//if there is an error during conversion, display it
		ROS_ERROR("tutorialROSOpenCV::main.cpp::cv_bridge exception: %s", e.what());
		return;
	}
	//image processing section
	cv::Mat src = corridorProces(cv_ptr->image) ;
/*	if (cv_ptr==NULL)
	{
		ROS_ERROR("NULL pointer at src \n");
		exit(1);
	}	*/	
	// row col 320 640 ================!!!!!!!!!!! iF	
	        
	//Display the image using OpenCV
	cv::imshow(WINDOW, src);
	//Add some delay in miliseconds. The function only works if there is at least one HighGUI window created and the window is active. If there are several HighGUI windows, any of them can be active.
	cv::waitKey(3);
	//Convert the CvImage to a ROS image message and publish it on the "camera/image_processed" topic.	
	cv_ptr->image=src;
	pub.publish(cv_ptr->toImageMsg());
}


int main(int argc, char **argv)
{	
	ros::init(argc, argv, "image_processor_corridor");
	ros::NodeHandle nh;
	//Create an ImageTransport instance, initializing it with our NodeHandle.
	image_transport::ImageTransport it(nh);
	//OpenCV HighGUI call to create a display window on start-up.
	cv::namedWindow(WINDOW, CV_WINDOW_AUTOSIZE);
	//	cv::namedWindow("Hough4", CV_WINDOW_AUTOSIZE);
	image_transport::Subscriber sub = it.subscribe("ardrone/image_rect_color", 1, imageCallback);
	//OpenCV HighGUI call to destroy a display window on shut-down.
	cv::destroyWindow(WINDOW);	
	pub = it.advertise("camera/image_processed_corridor", 1);	
	ros::spin();
	//ROS_INFO is the replacement for printf/cout.
	ROS_INFO("tutorialROSOpenCV::main.cpp::No error.");
	while(ros::ok())
	{
	}
	
}
