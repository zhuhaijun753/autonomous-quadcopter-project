

$ roslaunch cvg_sim_test railway_straight.launch
$ rosrun RailDetect railDetect
$ rosrun RailFlyControl2 railFly
=========================
$ rosbag record gazebo/model_states cmd_vel /railway/fly_command

PID in pkf RailFlyControl2 

Grid:
P=0.002...0.01
D=0.001...0.01

Time stamp example in pkg RailDetect 
	//double secs;
	//secs = ros::Time::now().toSec()-secs;
	//printf( "running time using %f \n",secs);
	//secs =ros::Time::now().toSec();
	