#include <ros/ros.h>
#include <iostream>
#include <fstream>
#include <stdio.h>
#include <cmath>
#include "ardrone_autonomy/Navdata.h"
#include <geometry_msgs/Twist.h>
#include <RailDetect/RailFlyCommand.h>
#include <std_msgs/Empty.h>
#include <std_srvs/Empty.h>

#define DISTANCE_H 200	
#define DISTANCE_W 1400
#define CENTER_X 500
#define CENTER_Y 500
#define SPEED 0.1

#undef PID_TEST
// #define TEST
using namespace ros;
using namespace std;


Publisher pub;// = node.advertise<geometry_msgs::Twist>("cmd_vel", 1);
Publisher pub_land;
float rotationZref=0.0, rotationZ=0.0;
bool setRotationRef=false; // true when the drone took off and stabilized 
bool ctrCall= false;
// PID gains
//float P=0.004275, I=0.0, D=0.0; // ZN
//float P=0.00684, I=0.0,	D=0.00684; // ZN
//float P=0.002, I=0.0,	D=0.002; // Me after ZN
float P=0.002, I=0.0, D=0.004; // ZN


float errorLast = 0.0;
//<added>
float totalError = 0.0;
int ctrlSteps = 0;
//</added>
float Ierror = 0.0;
void controlCallback(const RailDetect::RailFlyCommand::ConstPtr& usm)
{
	// Set default command to hover, unles there is no need for change rotation or corridor fly
	
	std_msgs::Empty msgEmpty;
	geometry_msgs::Twist commandTmp;
	commandTmp.linear.x = 0.0;
	commandTmp.linear.y = 0.0;
	commandTmp.linear.z = 0.0;
	commandTmp.angular.z = 0.0;
	float Dtime=usm->Dtime;
	float error = 320-usm->estimationX;
	//<added>
	totalError += abs(error*error);
	ctrlSteps++;
	//</added>	
	if (usm->observationX <0) error=errorLast;
	float Derror=(error-errorLast)/Dtime;
	Ierror = Ierror + error*Dtime;
	float speed = error*P + Ierror*I + Derror*D;

	if (abs(speed) > 1 ) speed=speed/speed;

	if ( error< 0)
	{
		commandTmp.angular.z = speed;	
	} else
	{
		if (error > 0 )
			commandTmp.angular.z = speed;
	}

	commandTmp.linear.x = 0.9;

	if (usm->ahead == "land")  pub_land.publish(msgEmpty);
	pub.publish(commandTmp);
}
void rotStabilizationCallback(const ardrone_autonomy::Navdata::ConstPtr& usm)
{
	
	if (setRotationRef) 
	{
		rotationZref=(usm->rotZ)+180;
		setRotationRef=false;
	}else{
		rotationZ=(usm->rotZ)+180;
	}
}


int main(int argc, char **argv) {
	//  Initialize the node
	init(argc, argv, "railway_fly");
	//<added>
	if (argc>1)
	{
		P = atof(argv[1]);
		I = atof(argv[2]);
		D = atof(argv[3]);		
		printf("P=%f\n",P);
		printf("I=%f\n",I);
		printf("D=%f\n\n",D);				
	}
	//</added>
	
	//  Create a node handle
	NodeHandle node;
	//  Subscribe to the corridor fly command data
	Subscriber sub = node.subscribe("/railway/fly_command", 1, controlCallback);
	//  Subscribe navigational data for stabilizing the rotational around z axes
	Subscriber sub2 = node.subscribe("/ardrone/navdata", 1, rotStabilizationCallback);
	//  A publisher for the movement data 
	pub = node.advertise<geometry_msgs::Twist>("cmd_vel", 1);
	//  Msg for fly control commands
	geometry_msgs::Twist command;
	command.linear.x = 0.0;
	command.linear.y = 0.0;
	command.linear.z = 0.0;
	command.angular.z = 0.0;
	//  msg for takeoff and landing 
	std_msgs::Empty msgEmpty;
	Publisher pub_takeoff = node.advertise<std_msgs::Empty>("/ardrone/takeoff", 1, true);
	pub_land = node.advertise<std_msgs::Empty>("/ardrone/land", 1, true);  
	//  Service for flat trim and imu calibration, wait 3sec.
// for simulation comment this section	
//	system( "rosservice  call ardrone/imu_recalib" );
//	system( "rosservice  call ardrone/flattrim" );
//	Duration(3).sleep();	
	//  take off and wait for 10 sec for stabilization and stuff(use landing zone under QC)  
	//cout<< "Take Off";// << endl;	
	spinOnce();
	Duration(3).sleep(); // normally 10 sec should be 
	setRotationRef  =true;
	Duration(3).sleep();
printf("init hardware done \n");
	ctrCall=true;	
	Rate rate(100);
#ifndef PID_TEST
	while (ok()) {
		//pub.publish(command);
		rate.sleep();
		ros::spinOnce();
	}
#endif
#ifdef PID_TEST
	//<added>
	double startTime=ros::Time::now().toSec();
	double totalTime = 10;

	while (ok() && ros::Time::now().toSec()-startTime<totalTime) {
	//</added>		
		rate.sleep();
		ros::spinOnce();
	}
	//  hover command
	command.linear.x = 0.0;
	command.linear.y = 0.0;
	command.linear.z = 0.0;
	command.angular.z = 0.0;	
	pub.publish(command);
	ros::spinOnce();

	printf("\n\nP=%f, I=%f, D=%f, rms_error=%f\n\n",P,I,D, sqrt(totalError)/ctrlSteps);		

	ofstream myfile;
	myfile.open ("example.txt",ios::out | ios::app);

  if (myfile.is_open())
  {
    myfile << "This is a line.\n";
    myfile << "This is another line.\n";
    myfile.close();
	printf("wrinting to file");
  }
  else cout << "Unable to open file";
  myfile.close();

#endif 
	//  landing commnad    	
	pub_land.publish(msgEmpty);  
	//<added>   

	ros::spinOnce();
	return 0;
}
